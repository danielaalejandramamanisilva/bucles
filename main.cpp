/*SENTENCIA WHILE
while(expresion logica){
    bloque de instrucciones
}
SENTENCIA DO WHILE
do {
    bloque de instrucciones
}while(expresion logica)
SENTENCIA FOR
for(expr1;expresion logica;expr2){
    bloque de instrucciones
}*/
#include <iostream>

using namespace std;

int main()
{
    /*TRANSFORMAR UN NUMERO ENTERO A ROMANOS
    M=1000
    D=500
    C=100
    L=50
    X=10
    int numero,unid,decena,centena,millar;
    cout<<"digite un numero: ";
    cin>>numero;
    //2151=2000+100+50+1
    unid=numero%10; numero=numero/10;//2151->1,numero=215
    decena=numero%10; numero/=10;//215->5, numero=21
    centena=numero%10; numero/=10;//1, numero=2
    millar=numero%10; numero/=10;//millar=2 numero=0
    switch(millar){
        case 1: cout<<"M"; break;
        case 2: cout<<"MM"; break;
        case 3: cout<<"MMM"; break;
    }
    switch(centena){
        case 1: cout<<"C"; break;
        case 2: cout<<"CC"; break;
        case 3: cout<<"CCC"; break;
        case 4: cout<<"CD"; break;
        case 5: cout<<"D"; break;
        case 6: cout<<"DC"; break;
        case 7: cout<<"DCC"; break;
        case 8: cout<<"DCCC"; break;
        case 9: cout<<"CM"; break;
    }
    switch(decena){
        case 1: cout<<"X"; break;
        case 2: cout<<"XX"; break;
        case 3: cout<<"XXX"; break;
        case 4: cout<<"XL"; break;
        case 5: cout<<"L"; break;
        case 6: cout<<"LX"; break;
        case 7: cout<<"LXX"; break;
        case 8: cout<<"LXXX"; break;
        case 9: cout<<"XC"; break;
    }
     switch(unid){
        case 1: cout<<"I"; break;
        case 2: cout<<"II"; break;
        case 3: cout<<"III"; break;
        case 4: cout<<"IV"; break;
        case 5: cout<<"V"; break;
        case 6: cout<<"VI"; break;
        case 7: cout<<"VII"; break;
        case 8: cout<<"VIII"; break;
        case 9: cout<<"IX"; break;
     }
    //calcular la suma de n numeros introducidos por teclado(caleb Erardo 5)
    int contador=0,suma=0, numero,n;
    cout<<"ingrese la cantidad de numeros: ";
    cin>>n;
    while(contador<n){
        cout<<"numero a sumar: "; cin>>numero;
        suma=suma+numero;
        contador++;
    }
    cout<<"la suma es: "<<suma;
    //calcular la suma de los primeros 100 numeros naturales
    int cont=1,suma=0;
    while(cont<=100){
        suma=suma+cont;
        cont++;
    }
    cout<<"la sumatoria de los primeros 100 numeros es: "<<suma;
    //calcular la suma de los numeros pares e impares entre 1 y n y verificar cuantos son pares e impares
    int limite, num,cont=0, par=0,impar=0;
    int conImp=0,conPar=0;
    cout<<"ingrese la cantidad de numeros a sumar: "; cin>>limite;
    while(cont<limite){
        cout<<"ingrese numero: ";
        cin>>num;
        if(num%2==0){
            par=par+num;//6+6=12
            conPar++;
        }else{
            impar=impar+num;//1+3=4
            conImp++;
        }
        cont++;
    }
    cout<<"suma de pares: "<<par<<" y son "<<conPar<<" pares"<<endl;
    cout<<"suma de impares: "<<impar<<" y son "<<conImp<<" impares"<<endl;
    //do while
    //determinar en n numeros: cuantos son menores que 20, mayores que 60 y cuantos estan entre 30 y 50
    int n, num;
    int cont=0,con20=0,con60=0,conRango=0;
    cout<<"ingrese la cantidad de numeros: "; cin>>n;
    do{
        cout<<cont+1<<".- ingresar numero: ";cin >>num;
        if(num<20){
            con20++;
        }
        if(num>60){
            con60++;
        }
        if(num>=30 && num<=50){
            conRango++;
        }
        cont++;
    }while(cont<n);
    cout<<"menores a 20: "<<con20<<endl;
    cout<<"mayores a 60: "<<con60<<endl;
    cout<<"entre 30 y 50: "<<conRango<<endl;*/
    /*los padres de una ni�a le prometen 10 dolares cuando cumpla 12 a�os y duplicar el regalo cada cumplea�os
    hasta que el regalo exceda 1000 dolares. escriba un progama para determinar que edad tendra la ni�a cuando
    se le de la ultima cantidad y cuanto dinero en total recibe
    int edad=12, d=10,total=10;
    cout<<"\t\tCalculo de dinero recibido\n\n";
    do {
        d=d*2;
        total=total+d;
        edad++;
    }while(total<1000);
    cout<<"edad: "<<edad<<endl;
    cout<<"dinero recibido: "<<d<<endl;
    cout<<"total: "<<total;*/
    //desarrolle un programa que convierta galones a litros, el programa debe mostrar galones 10 a 20 en incremento
    //1 galon contiene 3.785 litros(caleb 5 )
    double x,y;
    x=3.785;
    y=1;
    do{
        cout<<y<<" galones contienen: "<<y*x<<"litros"<<endl;
        y++;

    }while(y<=20);
    return 0;
}
